﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Создайте класс ArrayList. Реализуйте в простейшем приближении возможность использования его 
экземпляра аналогично экземпляру класса ArrayList из пространства имен System.Collections. 
*/

namespace _2._11_Task_4
{
    class Program
    {
        static void Main()
        {
            MyArrayList list = new MyArrayList();

            list.Add(1);
            list.Add("add");
            list.Add(0.789);
            list.Add('a');

            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine(list[i]);
            }

            list.Clear();

            Console.ReadKey();
        }
    }
}
